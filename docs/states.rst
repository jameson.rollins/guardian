.. _states:

Guardian `States`
=================

Guardian `states` are class definitions that inherit from the
:class:`~guardian.GuardState` base class.  A
:class:`~guardian.GuardState` has two methods, that are overridden to
program the state behavior::

  class DAMPED(GuardState):

      # main method executed once
      def main(self):
          ...

      # run method executed in a loop
      def run(self):
          ...

The methods can execute any arbitrary code you like.


state execution model
---------------------	  

Below is a simplistic overview of the state execution model::

  state = system.STATE2()

  init = True
  while True:
      if init:
          method = state.main
          init = False
      else:
          method = state.run

      status = method()

      if status:
          break

The first iteration of the loop executes the :py:meth:`main()` method,
whereas subsequent iterations execute the :py:meth:`run()` method.  If
either method returns :py:const:`True`, or a :py:obj:`str` name of
different state, the state is considered "done", and guardian will
then proceed in it's path through the graph.


The :class:`GuardState` class
-----------------------------

.. autoclass:: guardian.GuardState
   :members:
