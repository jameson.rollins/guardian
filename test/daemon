#!/usr/bin/env bash

test_description='daemon basic execution'

. lib/test-lib.sh

################################################################

test_begin_subtest "INIT with no request"
testioc ${IFO}:TEST- &
sleep .1
guardian --ca-prefix=TEST --name=TEST TEST0 &
wait_node_ready TEST
cawait T1:GRD-TEST_STATUS DONE
caget -t T1:GRD-TEST_STATE >OUTPUT
caget -t T1:GRD-TEST_REQUEST >>OUTPUT
caget -t T1:GRD-TEST_NOMINAL >>OUTPUT
caget -t T1:GRD-TEST_OK >>OUTPUT
kill_jobs
cat <<EOF >EXPECTED
A
NONE
NONE
False
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "request state"
testioc ${IFO}:TEST- &
sleep .1
guardian --op=PAUSE --ca-prefix=TEST --name=TEST TEST0 &
wait_node_ready TEST
camonitor -tn -F. T1:GRD-TEST_STATE >OUTPUT_R &
camonitor -tn -F. T1:GRD-TEST_STATE_N >OUTPUT_RN &
camonitor -tn -F. T1:GRD-TEST_STATE_S >OUTPUT_RS &
sleep .1
caput -c T1:GRD-TEST_REQUEST C
caput -c T1:GRD-TEST_OP EXEC
cawait T1:GRD-TEST_STATE C
kill_jobs
cat OUTPUT_R{,N,S} >OUTPUT
cat <<EOF >EXPECTED
T1:GRD-TEST_STATE..INIT..
T1:GRD-TEST_STATE..A..
T1:GRD-TEST_STATE..B..
T1:GRD-TEST_STATE..C..
T1:GRD-TEST_STATE_N..0..
T1:GRD-TEST_STATE_N..1..
T1:GRD-TEST_STATE_N..2..
T1:GRD-TEST_STATE_N..3..
T1:GRD-TEST_STATE_S..INIT..
T1:GRD-TEST_STATE_S..A..
T1:GRD-TEST_STATE_S..B..
T1:GRD-TEST_STATE_S..C..
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "request inherited state"
testioc ${IFO}:TEST- &
sleep .1
guardian --op=PAUSE --ca-prefix=TEST --name=TEST TEST1 &
wait_node_ready TEST
camonitor -tn -F. T1:GRD-TEST_STATE >OUTPUT &
sleep .1
caput -c T1:GRD-TEST_REQUEST D
caput -c T1:GRD-TEST_OP EXEC
cawait T1:GRD-TEST_STATE D
kill_jobs
cat <<EOF >EXPECTED
T1:GRD-TEST_STATE..INIT..
T1:GRD-TEST_STATE..A..
T1:GRD-TEST_STATE..B..
T1:GRD-TEST_STATE..C..
T1:GRD-TEST_STATE..D..
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "inspect intermediate state"
testioc ${IFO}:TEST- &
sleep .1
guardian --op=PAUSE --ca-prefix=TEST --name=TEST TEST1 &
wait_node_ready TEST
caput -c T1:GRD-TEST_REQUEST A
caput -c T1:GRD-TEST_OP EXEC
cawait T1:GRD-TEST_STATE A
cawait T1:GRD-TEST_STATUS DONE
node_dump_state TEST > OUTPUT
kill_jobs
cat <<EOF >EXPECTED
T1:GRD-TEST_ACTIVE.True
T1:GRD-TEST_ERROR.False
T1:GRD-TEST_INTENT.False
T1:GRD-TEST_MANAGER.
T1:GRD-TEST_MODE.AUTO
T1:GRD-TEST_NOMINAL.D
T1:GRD-TEST_NOMINAL_N.100
T1:GRD-TEST_NOMINAL_S.D
T1:GRD-TEST_OK.False
T1:GRD-TEST_OP.EXEC
T1:GRD-TEST_READY.True
T1:GRD-TEST_REQUEST.A
T1:GRD-TEST_REQUEST_N.1
T1:GRD-TEST_REQUEST_S.A
T1:GRD-TEST_STALLED.False
T1:GRD-TEST_STATE.A
T1:GRD-TEST_STATE_N.1
T1:GRD-TEST_STATE_S.A
T1:GRD-TEST_STATUS.DONE
T1:GRD-TEST_SUBNODES_NOT_OK.0
T1:GRD-TEST_SUBNODES_TOTAL.0
T1:GRD-TEST_TARGET.A
T1:GRD-TEST_TARGET_N.1
T1:GRD-TEST_TARGET_S.A
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "inspect final state"
testioc ${IFO}:TEST- &
sleep .1
guardian --op=PAUSE --ca-prefix=TEST --name=TEST TEST1 &
wait_node_ready TEST
caput -c T1:GRD-TEST_REQUEST D
caput -c T1:GRD-TEST_OP EXEC
cawait T1:GRD-TEST_STATE D
cawait T1:GRD-TEST_OK True
node_dump_state TEST > OUTPUT
kill_jobs
cat <<EOF >EXPECTED
T1:GRD-TEST_ACTIVE.True
T1:GRD-TEST_ERROR.False
T1:GRD-TEST_INTENT.True
T1:GRD-TEST_MANAGER.
T1:GRD-TEST_MODE.AUTO
T1:GRD-TEST_NOMINAL.D
T1:GRD-TEST_NOMINAL_N.100
T1:GRD-TEST_NOMINAL_S.D
T1:GRD-TEST_OK.True
T1:GRD-TEST_OP.EXEC
T1:GRD-TEST_READY.True
T1:GRD-TEST_REQUEST.D
T1:GRD-TEST_REQUEST_N.100
T1:GRD-TEST_REQUEST_S.D
T1:GRD-TEST_STALLED.False
T1:GRD-TEST_STATE.D
T1:GRD-TEST_STATE_N.100
T1:GRD-TEST_STATE_S.D
T1:GRD-TEST_STATUS.DONE
T1:GRD-TEST_SUBNODES_NOT_OK.0
T1:GRD-TEST_SUBNODES_TOTAL.0
T1:GRD-TEST_TARGET.D
T1:GRD-TEST_TARGET_N.100
T1:GRD-TEST_TARGET_S.D
EOF
test_expect_equal_file OUTPUT EXPECTED

################################################################

test_done
