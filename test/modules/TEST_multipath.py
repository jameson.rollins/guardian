from guardian import GuardState

class A(GuardState):
    def main(self):
        ezca['A'] = 1
        return True

class B1(GuardState):
    def main(self):
        ezca['B'] = 1
        return True

class B2(GuardState):
    def main(self):
        ezca['B'] = 2
        return True

class C1(GuardState):
    def main(self):
        ezca['C'] = 1
        return True

class C2(GuardState):
    def main(self):
        ezca['C'] = 2
        return True

class D(GuardState):
    def main(self):
        ezca['D'] = 1
        return True

edges = [
    ('A', 'B1'),
    ('B1', 'B2'),
    ('B2', 'D'),
    ('A', 'C1'),
    ('C1', 'C2', 2),
    ('C2', 'D'),
    ]
