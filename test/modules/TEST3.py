from guardian import GuardState

nominal = 'C'

class A(GuardState):
    pass

class B(GuardState):
    pass

class C(GuardState):
    pass

edges = [
    ('INIT', 'A'),
    ('A', 'B'),
    ('B', 'A'),
    ('B', 'C'),
    ]
