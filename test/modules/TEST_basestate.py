from guardian import GuardState

class _BaseState(GuardState):
    pass

class X(_BaseState):
    arm = 'X'

class Y(_BaseState):
    arm = 'Y'
