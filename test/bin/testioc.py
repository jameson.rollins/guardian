import sys
import pcaspy
from pprint import pprint

prefix = sys.argv[1]
pvdb = {
    'ALARM': {
        'prec': 0
    },
    'A': {
        'prec': 0
    },
    'B': {
        'prec': 0
    },
    'C': {
        'prec': 0
    },
    'D': {
        'prec': 0
    },
    'J': {
        'prec': 0
    },
    'K': {
        'prec': 0
    },
}

class DummyDriver(pcaspy.Driver):
    def read(self, channel):
        value = self.getParam(channel)
        print('%s == %s' % (channel, value))
        return value

    def write(self, channel, value):
        print('%s => %s' % (channel, value))
        self.setParam(channel, value)
        self.updatePVs()
        return True

if __name__ == '__main__':
    server = pcaspy.SimpleServer()

    # print('prefix: ' + prefix)
    # print('db: ')
    # pprint(pvdb)
    print("pcas channels:")
    for chan in pvdb:
        print('  {}{}'.format(prefix, chan))

    server.createPV(prefix, pvdb)
    driver = DummyDriver()

    for k in pvdb:
        driver.setParamStatus(k, pcaspy.Severity.NO_ALARM, pcaspy.Severity.NO_ALARM)
    driver.updatePVs()

    print("ready:")
    while True:
        server.process(0.1)
